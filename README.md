Installing Angular CLI<br><br>
Major versions of Angular CLI follow the supported major version of Angular, but minor versions can be released separately.

Install the CLI using the npm package manager:<br>
    <b>npm install -g @angular/cli</b>

<i>[Build From Scratch]</i><br>
To create a new workspace and an initial application project:

1. Ensure that you are not already in an Angular workspace folder. For example, if you have previously created the Getting Started workspace, change to the parent of that folder.

2. Run the CLI command ng new and provide the name angular-tour-of-heroes, as shown here:
    
    <b>ng new angular-tour-of-heroes</b>

3. The ng new command prompts you for information about features to include in the initial application project. Accept the defaults by pressing the Enter or Return key.

<br>

<i>[Or Just...]</i><br>
Pull this git into your folder.
<br><br>



Source: https://angular.io/tutorial
